<?php

use app\components\Router;

define('ROOT', dirname(__FILE__));

require __DIR__ . '/vendor/autoload.php';

$router = new Router;
$router->run();
