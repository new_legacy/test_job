<?php

use app\models\Employee;
use app\models\Organization;

/** @var Employee[] $employees */
/** @var Organization $organization */
?>
<!doctype html>
<html lang="ru">
<head>
    <!-- Required meta tags -->
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">

    <!-- Bootstrap CSS -->
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.5.0/css/bootstrap.min.css"
          integrity="sha384-9aIt2nRpC12Uk9gS9baDl411NQApFmC26EwAOH8WgZl5MYYxFfc+NcPb1dKGj7Sk" crossorigin="anonymous">
    <title>Список сотрудников организации <?php echo $organization['name'] ?></title>
</head>
<body>
    <div class="container">
        <?php if($employees) :?>
        <h1>Список сотрудников организации <?php echo $organization['name'] ?></h1>
            <table class="table table-striped">
                <thead>
                <tr>
                    <th scope="col">#</th>
                    <th scope="col">ФИО</th>
                    <th scope="col">Email</th>
                    <th scope="col">Дата рождения</th>
                </tr>
                </thead>
                <tbody>
                <?php
                    $i = 1;
                    foreach ($employees as $employee) :?>
                        <tr>
                            <th scope="row"><?php echo $i; ?></th>
                            <td><?php echo $employee['full_name']; ?></td>
                            <td><?php echo $employee['email']; ?></td>
                            <td><?php echo date("d.m.Y",strtotime($employee['date_of_birth'])) ?></td>
                        </tr>
                    <?php
                    $i++;
                endforeach;
                ?>
                </tbody>
            </table>
        <?php endif; ?>
    </div>
<!-- Optional JavaScript -->
<!-- jQuery first, then Popper.js, then Bootstrap JS -->
<script src="https://code.jquery.com/jquery-3.5.1.slim.min.js"
        integrity="sha384-DfXdz2htPH0lsSSs5nCTpuj/zy4C+OGpamoFVy38MVBnE+IbbVYUew+OrCXaRkfj"
        crossorigin="anonymous"></script>
<script src="https://cdn.jsdelivr.net/npm/popper.js@1.16.0/dist/umd/popper.min.js"
        integrity="sha384-Q6E9RHvbIyZFJoft+2mJbHaEWldlvI9IOYy5n3zV9zzTtmI3UksdQRVvoxMfooAo"
        crossorigin="anonymous"></script>
<script src="https://stackpath.bootstrapcdn.com/bootstrap/4.5.0/js/bootstrap.min.js"
        integrity="sha384-OgVRvuATP1z7JjHLkuOU7Xw704+h835Lr+6QL9UvYjZE3Ipu6Tp75j7Bh/kR0JKI"
        crossorigin="anonymous"></script>
</body>
</html>