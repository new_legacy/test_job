<?php

return array(
    'admin/upload-success' => 'admin/uploadSuccess',
    'admin/upload' => 'admin/upload',
    'admin/login' => 'admin/login',
    'admin' => 'admin/index',
    'organization/([a-zA-Z0-9\$\.]+)' => 'site/organization/$1',
    '' => 'site/index'
);