<?php


namespace app\models;


use app\components\Db;
use PDO;

class Employee
{
    public $id_organization;
    public $full_name;
    public $email;
    public $date_of_birth;

    public function save()
    {
        if(!$this->checkEmployee()){
            $db = Db::getConnection();
            $sql = "INSERT INTO employee (id_organization, full_name, email, date_of_birth) VALUES (?,?,?,?)";
            $result = $db -> prepare($sql);
            $result->execute([$this->id_organization, $this->full_name, $this->email, $this->date_of_birth]);
        }

        return true;
    }

    public function checkEmployee()
    {
        $db = Db::getConnection();

        $sql = "SELECT * FROM employee WHERE id_organization = ? AND full_name = ? AND email = ? AND date_of_birth = ?";
        $result = $db -> prepare($sql);
        $result->execute([$this->id_organization, $this->full_name, $this->email, $this->date_of_birth]);
        $result = $result->fetch(PDO::FETCH_ASSOC);

        return $result;
    }

    public static function getEmployeesByIdOrganization($id_organization)
    {
        $db = Db::getConnection();

        $sql = "SELECT * FROM employee WHERE id_organization = ?";
        $result = $db -> prepare($sql);
        $result->execute([$id_organization]);
        $result = $result->fetchAll(PDO::FETCH_ASSOC);

        return $result;
    }
}