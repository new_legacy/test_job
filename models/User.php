<?php

namespace app\models;

use app\components\Db;
use PDO;

class User
{
    public $login;
    public $password;

    public function __construct($login, $paasword)
    {
        $login = htmlspecialchars($login);
        $login = strip_tags($login);

        $this->login = $login;
        $this->password = $paasword;
    }

    public function Login()
    {
        $db = Db::getConnection();
        $sql = "SELECT * FROM user WHERE login = ?";
        $result = $db->prepare($sql);
        $result->execute([$this->login]);
        $result = $result->fetch(PDO::FETCH_ASSOC);

        if (password_verify($this->password, $result['password'])) {
            setcookie("User", true, time() + 3600 * 24 * 30, '/');
            return true;
        }

        return false;
    }

    public static function checkUser()
    {
        return isset($_COOKIE['User']) ? true : false;
    }
}