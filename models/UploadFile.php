<?php
namespace app\models;

use PhpOffice\PhpSpreadsheet\Reader\Xlsx;
use PhpOffice\PhpSpreadsheet\Shared\Date;

class UploadFile
{
    public static function Upload($file){
        //$sFile = ROOT . '/web/uploads/file.xlsx';
        $sFile = $file;
        $oReader = new Xlsx();

        $oSpreadsheet = $oReader->load($sFile);
        $oCells = $oSpreadsheet->getActiveSheet()->getCellCollection();

        for ($iRow = 2; $iRow <= $oCells->getHighestRow(); $iRow++) {

            $organization_cell = $oCells->get('A' . $iRow);
            $full_name = $oCells->get('B' . $iRow);
            $email = $oCells->get('C' . $iRow);
            $date_of_birth = $oCells->get('D' . $iRow);

            $organization = Organization::checkOrganization($organization_cell->getValue());
            $id_organization = $organization['id'];

            if (!$organization) {
                $model = new Organization();
                $model->name = $organization_cell->getValue();
                $id_organization = $model->save();
            }

            $model = new Employee();
            $model->id_organization = $id_organization;
            $model->full_name = $full_name->getValue();
            $model->email = $email->getValue();

            $date_of_birth = Date::excelToTimestamp($date_of_birth->getValue());
            $date_of_birth = date('Y.m.d', $date_of_birth);
            $model->date_of_birth = $date_of_birth;

            $model->save();

        }
    }
}