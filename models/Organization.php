<?php


namespace app\models;

use app\components\Db;
use PDO;

class Organization
{
    public $name;
    public $link;

    public function save()
    {
        $this->link = $this->generateLink();

        $db = Db::getConnection();
        $sql = "INSERT INTO organization (name, link) VALUES (?,?)";
        $result = $db -> prepare($sql);
        $result->execute([$this->name, $this->link]);

        return $db->lastInsertId();
    }

    public function generateLink()
    {
        $hash = crypt($this->name);

        while(strstr($hash, '/')){
            $hash = crypt($this->name);
        }

        return $hash;
    }

    public static function checkOrganization($name)
    {
        $db = Db::getConnection();

        $sql = "SELECT id, name FROM organization WHERE name = ?";
        $result = $db -> prepare($sql);
        $result->execute([$name]);
        $result = $result->fetch(PDO::FETCH_ASSOC);

        return $result;
    }

    public static function getOrganizationByLink($link)
    {
        $db = Db::getConnection();

        $sql = "SELECT id FROM organization WHERE link = ?";
        $result = $db -> prepare($sql);
        $result->execute([$link]);
        $result = $result->fetch(PDO::FETCH_ASSOC);

        return $result;
    }

    public static function getOrganizations()
    {
        $db = Db::getConnection();

        $sql = "SELECT * FROM organization";
        $result = $db -> prepare($sql);
        $result->execute();
        $result = $result->fetchAll(PDO::FETCH_ASSOC);

        return $result;
    }
}