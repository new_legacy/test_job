<?php


namespace app\controllers;

use app\models\Organization;
use app\models\UploadFile;
use app\models\User;

class AdminController
{
    public function actionIndex()
    {
        if(!USer::checkUser()){
            header('Location: /admin/login');
        }

        $organizations = Organization::getOrganizations();
        return require_once (ROOT . '/views/admin/organizations-list.php');
    }

    public function actionUpload()
    {
        if(!USer::checkUser()){
            header('Location: /admin/login');
        }

        if($_FILES){
            $uploaddir = ROOT . '/web/uploads/';
            $uploadfile = $uploaddir . basename($_FILES['userfile']['name']);

            if (move_uploaded_file($_FILES['userfile']['tmp_name'], $uploadfile)) {
                UploadFile::Upload($uploadfile);
                header('Location: /admin/upload-success');
            } else {
                 die("Ошибка загрузки\n");
            }
        }
        return require_once (ROOT . '/views/admin/upload.php');
    }

    public function actionUploadSuccess()
    {
        if(!USer::checkUser()){
            header('Location: /admin/login');
        }

        return require_once (ROOT . '/views/admin/upload-success.php');
    }

    public function actionLogin()
    {
        if($_POST){
            $login = $_POST['login'];
            $password = $_POST['password'];

            $user = new User($login, $password);

            if($user->Login()){
                header('Location: /admin/');
            }
            else{
                die('Неверные данные');
            }
        }
        return require_once (ROOT . '/views/admin/login.php');
    }
}