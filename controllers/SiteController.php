<?php

namespace app\controllers;


use app\models\Employee;
use app\models\Organization;

class SiteController
{
    public function actionIndex()
    {
        echo '<h1>Главная страница</h1>';
        return true;
    }

    public function actionOrganization($link)
    {
        $organization = Organization::getOrganizationByLink($link);
        $employees = Employee::getEmployeesByIdOrganization($organization['id']);

        return require_once (ROOT . '/views/site/employee-list.php');
    }
}